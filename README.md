# Tech Test

#### Description



#### Catalog
 
1. [Data exchange by Ethernet/IP protocol between Modicon TM241 and Ormon CJ2M.](#1.EIP_TM241_CJ2M)



<a name="1.EIP_TM241_CJ2M"></a>
## 1.EIP_TM241_CJ2M


#### TM241与CJ2M的EIP数据交换
本实验介绍了TM241与CJ2M的如何进行Ethernet/IP数据交换。


##### 实验步骤：
1.在somachine v4.3编程软件中配置TM241的IP地址，EIP从站  

在设备树中选择Ethernet_1 设置IP地址：192.168.0.50 
![设置TM241的IP地址](https://gitee.com/uploads/images/2018/0611/094602_9e5df2c9_1742927.jpeg "TM241_IP地址.jpg")  

右键Ethernet_1 添加设备 选择EthernetIP  
![设置Ethernet/IP从站](https://gitee.com/uploads/images/2018/0611/094751_962d167e_1742927.jpeg "EIP从站.jpg") 

选择EthernetIP 修改EthernetIP从站的参数 将需要读写的数据字的大小写入输入区和输出区 这里读写的数据都是30个字
![修改从站参数](https://gitee.com/uploads/images/2018/0611/095004_8df553ad_1742927.jpeg "修改参数.jpg") 

最后将工程下载到PLC中。

2.在CX-Programmer编程软件中配置CJ2M的IP地址 

配置CJ2M的IP地址：192.168.0.48并且调整CPU本体上的拨码开关，十进制48转换为十六进制的30
![配置CJ2M](https://gitee.com/uploads/images/2018/0611/102726_037d022f_1742927.jpeg "CJ2M_IP地址.jpg")

将程序配置下载到PLC中。

3.在Network Configurator中组态网络

首先安装TM241的EDS文件，安装完成后进行配置。

添加CJ2M-EIP21和TM241并且修改Node Address 
![修改node address](https://gitee.com/uploads/images/2018/0611/103922_b62348e1_1742927.jpeg "Network_1.jpg") 

双击TM241图标 修改Size2 和 Size3的参数 之前在somachine中配置了输入输出都是30个字 所以这里都修改为60 单位为字节 然后确定
![修改241参数](https://gitee.com/uploads/images/2018/0611/104224_1237a073_1742927.jpeg "Network_2.jpg") 

双击CJ2M图标 选择Tag Sets 单击Edit Tags 新建标签 在In-Consume和Out-Produce中分别添加大小为60byte的标签D1000和D2000 单击确定
![](https://gitee.com/uploads/images/2018/0611/110548_7d601ab6_1742927.jpeg "Network_3.jpg")

![](https://gitee.com/uploads/images/2018/0611/110719_6f9dccfb_1742927.jpeg "Network_4.jpg")
 
双击CJ2M图标 选择Connections 将TM241从站加入Device List
![](https://gitee.com/uploads/images/2018/0611/105304_871bc049_1742927.jpeg "Network_5.jpg")

双击192.168.0.50 TM241编辑链接 

Connection I/O type选择 Read from 100/Write to 150 在Input和Output Tag set中选择之前设置的In和Out标签 然后单击Regist
![](https://gitee.com/uploads/images/2018/0611/110821_7984a181_1742927.jpeg "Network_6.jpg") 


配置完成如下图所示
![](https://gitee.com/uploads/images/2018/0611/110931_ee674b91_1742927.jpeg "Network_7.jpg")

在线将配置下载到PLC
![](https://gitee.com/uploads/images/2018/0611/110148_da355295_1742927.jpeg "Network_8.jpg") 

4.监控数据

在线监控TM241 在EthernetIP Slave I/O映射中 选择变量Output 修改部分值 这部分的值将被CJ2M读取
![](https://gitee.com/uploads/images/2018/0611/111648_57b2bacc_1742927.jpeg "监控数据-1.jpg") 

在线监控CJ2M 在内存中选择D区的D1000 监控数据
![](https://gitee.com/uploads/images/2018/0611/111807_a08cc8ef_1742927.jpeg "监控数据-2.jpg")

在线监控CJ2M 在内存中选择D区的D2000 写入数据 这部分的值将写入TM241
![](https://gitee.com/uploads/images/2018/0611/112118_70cad919_1742927.jpeg "监控数据-3.jpg")

在线监控TM241 在EthernetIP Slave I/O映射中 选择变量Input 监控数据
![](https://gitee.com/uploads/images/2018/0611/112250_6a37db85_1742927.jpeg "监控数据-4.jpg")




